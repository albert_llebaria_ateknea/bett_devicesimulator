const MongoClient = require('mongodb').MongoClient,
    test = require("assert");


const url = "mongodb://localhost:27017";
const dbName ="BettAir";
const collection ="SimulationResults";



const connect = (callback)=>{
    MongoClient.connect(url,function (err,client) {
        console.log(`CONECTING TO MONGO \n Host ${url} \n DB:${dbName}`);
        callback(client);
    });
}


const updateSimulation = (query,newvalues)=>{
    connect((client)=>{

        let col = client.db(dbName).collection(collection);
        col.updateOne(query,newvalues,(err, resp)=>{
            if(err)
                throw err;
            console.log("UPDATED")
        })
    })

};


const getSimulations =(callback)=>{
    connect((client)=>{
        let col = client.db(dbName).collection(collection);
        col.find({},{ _id: false, simulation: true, nodes: false }).toArray(function(err, result) {
            if (err) throw err;
            client.close();
            callback(result);
        });
    })

};

const getSimulationData = (simulationId,callback)=>{
    connect((client)=>{
        let col = client.db(dbName).collection(collection);

        col.find({'simulation_id':parseInt(simulationId)}).toArray(
            (err,item)=>{
            if (err) throw err;
            callback(item);
            client.close();

        })
    })
}

const createSimulationData = (simulationId,nNodes,time, datagrams,callback)=>{
    console.log("simulation id:"+simulationId);
    connect((client)=>{
        const col = client.db(dbName).collection(collection);
        col.insertOne({ "simulation_id": simulationId, sensors:nNodes, timeout: time, datagrams:datagrams, nodes:[] },(err,resp)=>{
            if (err)
                throw err;
            callback(client);
            client.close();
        });
    })
};

const createCollection =(callback)=>{
    connect(client=>{
        let dbo = client.db(dbName);
        dbo.createCollection(collection, function(err, res) {
            if (err) throw err;
            console.log("Collection created!");
            client.close();
        });

    })

};

module.exports= {
    getSimulations,
    updateSimulation,
    getSimulationData,
    createSimulationData,
    createCollection,
    connect
};

