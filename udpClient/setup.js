const reliableUDP = require("./main"),
    promises = require('promise')
mongoCLi = require('../mongo/simulationStats');


let sockets =[];

const setUpSockets = new Promise((resolve, reject)=>{

    mongoCLi.connect( client => {
        let count =0;
        let PORT =4000;


        let internalCallBack = (count)=>{
            if ( count <10 ){
                sockets.push(reliableUDP.configureSever(client,()=>{
                    console.log(`Socket ${count} has been configured`);
                }));
                sockets[count].server.bind(PORT);
                PORT++;
                count++;
                internalCallBack(count);
            }else{
                resolve(sockets);
            }
        };

        internalCallBack(0);

    });


});

setUpSockets.then(()=>{
    console.log("SERVERS HAVE BEEN CONFIGURED")
});


module.exports={
    sockets
};

