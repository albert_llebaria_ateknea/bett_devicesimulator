const dgram = require('dgram'),
    schedule = require('node-schedule'),
    config = require('./config').udpConfig,
    randomstring = require("randomstring"),
    crypto = require('crypto'),
    mongoCli = require('../mongo/simulationStats');


let intervals = [];
let max_transmit_waits = [];
let max_transmit_spans = [];
let timeouts = [];
let msgIDs = [];
const simulationResults = {
    resets: [],
    send: [],
    times: [],
    error: [],
    messages: [],
    max_transmit_wait: [],
    max_transmit_span: [],
};

const dbName = "BettAir";
const collection = "SimulationResults";


const configureSever = (dbClient, callback) => {
    let server = dgram.createSocket('udp4');


    let simulationId = 0;


    const changeSimulationId = (newId) => {
        simulationId = newId;
    };
    let getSimulationId = () => {
        console.log(simulationId);
    };
    // noinspection JSAnnotator
    ///############################## ON CLOSE  #############################################

    // FUNCTION CALLED WHEN THE UDP SERVER CLOSES
    server.on('close', () => {
        console.log(simulationId)
        console.log(simulationResults)
    });


    ///############################## ON MESSAGE  #############################################

// FUNCTION CALLED WHEN THE UDP SERVER RECIEVES A MSG
    server.on('message', (msg, rinfo) => {
        let receivedData = "";

        for (let i = 0; i < msg.length; i++) {
            receivedData += ("00" + msg[i].toString(16)).slice(-2);
        }


        let msgType = receivedData.substr(0, 2);
        let msgId = receivedData.substr(2, 8).toUpperCase();
        let nodeID = receivedData.substr(10, 4).toUpperCase();
        let payload = receivedData.substr(14, receivedData.length);
        console.log(`server got: MSG ${receivedData} TYPE ${msgType} with msgId ${msgId} ACK, from node '${nodeID} , with payload ${payload} '${rinfo.address}:${rinfo.port}`);


        if (!simulationResults.max_transmit_wait[msgId]) {
            if (msgType === '01') {
                simulationResults.error[msgId] = payload === '1';

                removeMsgIdFromBackUp(msgId);
                //updateDb(dbClient,simulationId,msgId,nodeID);

            } else if (msgType === '03') {

                receivedRST(msgId, server);

            } else if (msgType === '02') {

            } else {
                console.log(`NOT RECONIZED MSG TYPE ${receivedData}`)
            }
        }
    });


    ///############################## ON ERROR  #############################################
    server.on('error', (err) => {
        console.log(`server error:\n${err.stack}`);
        server.close();
    });


    ///############################## ON LISTENING  #############################################


    // FUNCTION CALLED WHEN SERVER STARTS LISTENING
    server.on('listening', () => {
        const address = server.address();
        console.log(`server listening ${address.address}:${address.port}`);
        callback();

    });


    return {
        server,
        simulationId,
        changeSimulationId,
        getSimulationId
    };

};


// FUNCTION THAT SENDS A UDP DATAGRAM
const sendMessage = (data, server) => {
    let sendingData = Buffer.from(data.msg, "hex");
    server.send(sendingData, 0, sendingData.length, data.serverPort, data.serverHost, function (err, bytes) {
        if (err) throw err;

        simulationResults.totalSend++;
        simulationResults.send[data.msgId]++;
        console.log('UDP message FROM ' + data.msg + ' sent to ' + data.serverHost + ':' + data.serverPort + '  Time ' + timeouts[data.msgId]);
    });

};


// MAIN FUNCTION THAT STARTS THE SIMULATION
const startSimulation = (waitingTime, simulationid, sensorID, nDatagrams, serverHost, serverPort, server) => {

    let i = 1;
    let simulationInterval = setInterval(() => {
        let messagePile = [];


        createMsgId((msgId) => {
            msgId = msgId.toUpperCase();
            msgIDs.push(msgId);


            let nodeId = '';
            //CREATING HEADERS PARAMS
            if (sensorID < 10) {
                nodeId = "C" + sensorID.toString() + "B2";
            } else if (sensorID >= 100 && sensorID < 1000) {
                nodeId = "C" + sensorID.toString();
            } else if (sensorID > 1000 && sensorID < 2000) {
                nodeId = sensorID.toString();
            }
            else {
                nodeId = "C" + sensorID.toString() + "2";
            }

            let data = createMessage("00", msgId, nodeId);

            let cliMsg = "";
            for (let info in data) cliMsg += data[info]
            console.log(data['epoch'])


            let sendingData = {
                msg: cliMsg,
                serverPort,
                serverHost,
                msgId,
                nodeId,
                simulationid

            };

            simulationResults.messages[msgId] = sendingData;

            //SETTING TRANSMISION CONTROL PARAMS
            simulationResults.times[msgId] = {sendAt: new Date().getTime(), responseAt: ''};
            simulationResults.send[msgId] = 0;
            simulationResults.error[msgId] = false;
            timeouts[msgId] = (Math.random() * (config.ack_timeout * config.ack_random_factor - 2000) + config.ack_timeout);
            simulationResults.resets[msgId] = 0;
            //DATA TRANSMISION AND CONTROL BY INTERVALS ID'S
            //
            setDeceleratingTimeout(sendingData, server);
        });

        i++;
        if (i >= nDatagrams) {
            console.log("CLEANING SIMULATION INTERVAL");
            clearInterval(simulationInterval);

        }
    }, waitingTime)
};


//FUNCTION THAT CREATS INTERVALS IN ORDER TO CREATE RETRANSMISION

function setDeceleratingTimeout(sendingData, server) {
    setMaxTransmitSpan(sendingData.msgId);
    setMaxTransmitWait(sendingData.simulationid, sendingData.nodeId, sendingData.msgId);


    let internalCallback = function (tick) {
        return function () {
            if (tick < config.max_retrasmit) {
                sendMessage(sendingData, server);
                timeouts[sendingData.msgId] *= 2;
                intervals[sendingData.msgId] = setTimeout(internalCallback, timeouts[sendingData.msgId]);
            }
        }
    }(simulationResults.send[sendingData.msgId]);

    intervals[sendingData.msgId] = setTimeout(internalCallback, timeouts[sendingData.msgId]);

}


const createMsgId = (callback) => {
    let created = false;
    let msgId;
    while (!created) {
        msgId = randomstring.generate({
            length: 8,
            charset: 'hex'
        });
        created = true;
        msgIDs.forEach((currentvalue) => {
            if (currentvalue === msgId)
                created = false;

        })
    }
    callback(msgId);
};

const removeMsgIdFromBackUp = (msgId) => {
    msgIDs.splice(msgIDs.indexOf(msgId), 1);
    simulationResults.times[msgId].responseAt = new Date().getTime();
    clearTimeouts(msgId);

};


const clearTimeouts = (msgID) => {
    clearTimeout(intervals[msgID]);
    clearTimeout(max_transmit_spans[msgID]);
    clearTimeout(max_transmit_waits[msgID]);
};
const setMaxTransmitWait = (simulationid, nodeId, msgId) => {
    simulationResults.max_transmit_wait[msgId] = false;

    max_transmit_waits[msgId] = setTimeout(() => {
        console.log(`${msgId} ARRIVED TO MAX TRANSMIT SPAN`);
        simulationResults.max_transmit_wait[msgId] = true;
        simulationResults.times[msgId].responseAt = 'x';


        let query = {"simulation_id": parseInt(simulationid)};
        let updateData = {
            id: msgId,
            max_transmit_span: simulationResults.max_transmit_span[msgId],
            max_transmit_wait: simulationResults.max_transmit_wait[msgId],
            send: simulationResults.send[msgId],
            error: simulationResults.error[msgId],
            times: simulationResults.times[msgId],
            sensor: nodeId,
            resets: simulationResults.resets[msgId]
        };

        let newvalues = {$push: {nodes: updateData}};
        mongoCli.updateSimulation(query, newvalues)


    }, config.max_transmit_wait);

};

const setMaxTransmitSpan = (msgId) => {
    simulationResults.max_transmit_span[msgId] = false;

    max_transmit_spans[msgId] = setTimeout(() => {
        console.log(`${msgId} ARRIVED TO MAX TRANSMIT SPAN`);
        simulationResults.max_transmit_span[msgId] = true;
        clearTimeout(intervals[msgId])

    }, config.max_transmit_span)

};

const receivedRST = (msgId, server) => {
    clearTimeout(intervals[msgId]);
    simulationResults.resets[msgId]++;

    setTimeout(() => {
        setDeceleratingTimeout(simulationResults.messages[msgId], server)
    }, 15000);

};

const LEDate = () => {
    let hexDate = Math.round((new Date()).getTime() / 1000).toString(16);
    let i = hexDate.length - 2;
    let LE = '';

    while (i >= 0) {
        LE += hexDate.substr(i, 2);
        i -= 2;
    }

    return LE;
};
const createMessage = (msgType, msgId, nodeId) => {
    return {
        msgType,
        flags: '00',
        msgId,
        nodeId,
        epoch: LEDate(),
        temperature1: randomValueHex(4),
        temperature2: randomValueHex(4),
        Lat: randomValueHex(8),
        Lon: randomValueHex(8),
        WE_SN1: randomValueHex(8),
        AE_SN1: randomValueHex(8),
        WE_SN2: randomValueHex(8),
        AE_SN2: randomValueHex(8),
        WE_SN3: randomValueHex(8),
        AE_SN3: randomValueHex(8),
        WE_SN4: randomValueHex(8),
        AE_SN4: randomValueHex(8),
        PM1: randomValueHex(8),
        PM10: randomValueHex(8),
        PM2: randomValueHex(8),
        presure: randomValueHex(8),
        relativeHumidity: randomValueHex(4),
        noisel01: randomValueHex(4),
        noisel02: randomValueHex(4),
        noisel03: randomValueHex(4),
        noisel04: randomValueHex(4),
        noisel05: randomValueHex(4),
        noiser01: randomValueHex(4),
        noiser02: randomValueHex(4),
        noiser03: randomValueHex(4),
        noiser04: randomValueHex(4),
        noiser05: randomValueHex(4),
        uvIndex: randomValueHex(4),
    }
};


const updateDb = (dbClient, simulationId, msgId, node) => {
    let col = dbClient.db(dbName).collection(collection);
    let query = {"simulation_id": simulationId};
    let updateData = {
        id: msgId,
        max_transmit_span: simulationResults.max_transmit_span[msgId],
        max_transmit_wait: false,
        send: simulationResults.send[msgId],
        error: simulationResults.error[msgId],
        times: simulationResults.times[msgId],
        sensor: node,
        resets: simulationResults.resets[msgId]
    };

    let newvalues = {$push: {nodes: updateData}};
    col.updateOne(query, newvalues, (err, resp) => {
        if (err)
            throw err;
        console.log("UPDATED")
    })
};


/** RANDOM GENERATION OF VALUES **/

function randomValueHex(len) {
    return crypto.randomBytes(Math.ceil(len / 2))
        .toString('hex') // convert to hexadecimal format
        .slice(0, len);   // return required number of characters
}


module.exports = {
    configureSever,
    startSimulation
};
