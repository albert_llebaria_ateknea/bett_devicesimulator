

module.exports.udpConfig = {
    ack_timeout:2000,
    ack_random_factor: 1.5,
    max_retrasmit: 4.0,
    nstart:1,
    default_leisure: 5,
    max_transmit_wait: 93000,
    max_transmit_span:45000
};