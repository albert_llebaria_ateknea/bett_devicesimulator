var express = require('express');
var router = express.Router();
const mongoCli = require('../mongo/simulationStats');
/* GET home page. */
router.get('/', function(req, res, next) {
    mongoCli.getSimulations((data)=>{
        //
        res.render('results', { title: 'Bett UDP Client', data: data });

    })
});


router.get('/:id', function(req, res, next) {
    mongoCli.getSimulationData(req.params.id,(data)=>{

        let avarageResponseTime = 0;
        let totalSend =0;
        let firedSpans=0;
        let firedWaits=0;
        let retransmissted=[];
        let max_retransmit=1;
        let not_received=0;
        let resets = 0;

        data[0].nodes.forEach((x)=>{

            if(x.max_transmit_wait){
                firedWaits++;
            }else if ( x.max_transmit_span){
                firedSpans++;
            }else{
                let aux=((2+4+8+16+32)*1.5)*1000;

                if(x.send>max_retransmit) max_retransmit=x.send;

                if(x.times.responseAt !== 'x') {
                    console.log(totalSend)
                    aux=parseInt(x.times.responseAt) - parseInt(x.times.sendAt);
                }else{
                    not_received++;
                }
                avarageResponseTime += aux;

                if(x.send>1) retransmissted.push({send: x.send, waitingTime:aux, id:x.id});
                resets+=x.resets;
            }
            totalSend+=x.send;
        });

        avarageResponseTime=(avarageResponseTime/totalSend).toFixed(2);

        let waitingTime =0;
        retransmissted.forEach((x)=>{
            waitingTime+=x.waitingTime;
        });
        if(retransmissted.length >0){
            waitingTime=(waitingTime/retransmissted.length).toFixed(2);
        }

        let suposedToSend = data[0].datagrams* data[0].sensors;
        res.send(JSON.stringify({avarageResponseTime,
            totalSend,
            firedSpans,
            firedWaits,
            suposedToSend,
            retransmissted,
            waitingTime,
            max_retransmit,
            notReceived: not_received,
            resets
        }));
    })


});

module.exports = router;
