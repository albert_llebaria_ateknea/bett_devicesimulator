const express = require('express'),
    mongoCLi = require('../../mongo/simulationStats'),
    startSimulation = require('../../udpClient/main').startSimulation,
    sockets = require("../../udpClient/setup").sockets;


let router = express.Router();

const node = function(n){
    this.id=n;
    this.send=0;
};


/* GET home page. */
router.post('/', function(req, res, next) {



    let n = parseInt(req.body.sensors);
    let simulationId= new Date().getTime();


    let datagrams = parseInt(req.body.datagrams);
    let host = req.body.host;
    let port = req.body.port;
    let time = parseInt(req.body.timeout)*1000;

    let prepareSimulation = new Promise((resolve,reject)=>{
        let dummies = [];
        for (let i=0;i<n;i++){
            dummies.push(new node(i));
            sockets[i].changeSimulationId(simulationId);

        }
        resolve(dummies)
    });

    mongoCLi.createSimulationData(simulationId,n,time,datagrams,()=>{});

    prepareSimulation.then((dummies)=>{
        for (let i=0;i<n;i++){
            startSimulation(time,simulationId, dummies[i].id, datagrams,host, port, sockets[i].server);
        }
    });

    let waitingTime = time*datagrams;
    waitingTime=waitingTime/60000;


    res.render('simulate', { title: 'Bett UDP Client' , subTitle:'Simulation', waitingTime });

});



module.exports = router;
